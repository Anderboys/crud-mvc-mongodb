package controlador;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelo.Empleado;
import modeloDAO.EmpleadoDAO;


@WebServlet("/Controlador")
public class Controlador extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	Empleado emp = new Empleado();
	EmpleadoDAO edao = new EmpleadoDAO();
	
	String idemp;
    Date date1;
    java.sql.Date date2;

	public Controlador() {
		super();
		// TODO Auto-generated constructor stub
	}

//	doGet
//	doPost
//    service
//    @Override

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {	

		String menu = request.getParameter("menu");
		String accion = request.getParameter("accion");

		System.out.println("valor de menu ->> " + menu);
		System.out.println("valor de accion ->> " + accion);

		if (menu.equals("Principalxx")) {
			request.getRequestDispatcher("principal.jsp").forward(request, response);
		}
		
		if (menu.equals("Producto")) {
			request.getRequestDispatcher("producto.jsp").forward(request, response);
		}

		if (menu.equals("Empleado")) {

			switch (accion) {			
			case "Listar":
				
				List lista = edao.listar();
				// enviar datos Attribute = empleados + objeto = emp
				request.setAttribute("empleados", lista);				        
		         
			break;
			

			case "Agregar":
				
				String ident=request.getParameter("txtIdent");
	            String nom=request.getParameter("txtNom");
	            String ape=request.getParameter("txtApe");
	            String dni=request.getParameter("txtDni");
	            Boolean enabled= Boolean.parseBoolean(request.getParameter("txtEnabled"));
	           
	            //PARSEAR DE STRING A DATE   -->> CORREGIDO <<--
	            String fecha =  request.getParameter("txtDate");
	            try {            
	                date1= new SimpleDateFormat("yyyy-MM-dd").parse(fecha);
	            } catch (ParseException ex) {
	                ex.printStackTrace();
	            }            
	            
	            emp.setIdentifier(ident);
	            emp.setNombre(nom);
	            emp.setApellido(ape);
	            emp.setDni(dni);
	            emp.setEnabled(enabled);
	            emp.setIssueDate(date1);
//	            e.setIssueDate(date2);
	            edao.agregar(emp); 
	
//				 Redirecciona el Listar
				request.getRequestDispatcher("Controlador?menu=Empleado&accion=Listar").forward(request, response);

				break;

			case "ListarByID":
				
				idemp=  request.getParameter("id");
				emp = edao.BuscarByID(idemp);
				request.setAttribute("empleadoXD", emp);
				
				request.getRequestDispatcher("Controlador?menu=Empleado&accion=Listar").forward(request, response);
				break;	

			case "Actualizar":
				
				String ident1=request.getParameter("txtIdent");
	            String nom1=request.getParameter("txtNom");
	            String ape1=request.getParameter("txtApe");
	            String dni1=request.getParameter("txtDni");
	            Boolean enabled1= Boolean.parseBoolean(request.getParameter("txtEnabled"));
	           
	            //PARSEAR DE STRING A DATE   -->> CORREGIDO <<--
	            String fecha1=  request.getParameter("txtDate");
	            try {
	                 date1= new SimpleDateFormat("yyyy-MM-dd").parse(fecha1);
	            } catch (ParseException ex) {
	                ex.printStackTrace();
	            }	
		
	            emp.setIdentifier(ident1);
	            emp.setNombre(nom1);
	            emp.setApellido(ape1);
	            emp.setDni(dni1);
	            emp.setEnabled(enabled1);
	            emp.setIssueDate(date1);
	            // UPDATE BY idemp
	            emp.setIdentifier(idemp);	            
	            edao.edit(emp); 	            

//				 Redirecciona el Listar
				request.getRequestDispatcher("Controlador?menu=Empleado&accion=Listar").forward(request, response);
				break;
				
		

			case "Delete":			
				idemp=  request.getParameter("id");
				  edao.eliminar(idemp);				
				request.getRequestDispatcher("Controlador?menu=Empleado&accion=Listar").forward(request, response);

				break;

			default:
				throw new AssertionError();
			}
			
			request.getRequestDispatcher("empleado.jsp").forward(request, response);

			
		}	
		
		if (menu.equals("EmpleadoListDNI")) {

			if (accion != null) {

				switch (accion) {

				case "ListarDNIxD":

					String dni2 = request.getParameter("txtdnixD");
					List lista2 = edao.listarByDNI(dni2);
					request.setAttribute("empleados", lista2);
					break;

				default:
					break;
				}
			}

			if (accion.equals("ListarDNIxD")) {
				request.getRequestDispatcher("empleadoitems.jsp").forward(request, response);
			} else {
				request.getRequestDispatcher("empleadoSearch.jsp").forward(request, response);
			}

		}


	}

}
