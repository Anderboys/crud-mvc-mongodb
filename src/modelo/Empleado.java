package modelo;

import java.util.Date;

public class Empleado {
	
    private String identifier;
    private String nombre;
    private String apellido;
    private String dni;
    private Boolean Enabled;
    private Date IssueDate;
    
    public Empleado() {
    }

	public Empleado(String identifier, String nombre, String apellido, String dni, Boolean enabled, Date issueDate) {
		super();
		this.identifier = identifier;
		this.nombre = nombre;
		this.apellido = apellido;
		this.dni = dni;
		Enabled = enabled;
		IssueDate = issueDate;
	}
	
	
	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public Boolean getEnabled() {
		return Enabled;
	}

	public void setEnabled(Boolean enabled) {
		Enabled = enabled;
	}

	public Date getIssueDate() {
		return IssueDate;
	}

	public void setIssueDate(Date issueDate) {
		IssueDate = issueDate;
	}

   
	

}
