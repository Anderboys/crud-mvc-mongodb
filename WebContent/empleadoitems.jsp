<!--   Para trabajar con   ->  c:forEach  <-   -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>

<%@ page import="java.util.List" %>
<%@ page import="java.util.Iterator" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>empleado items</title>

			<!--    THIS IS JQUERY   -->
			<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">				
			<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
			<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

          	     <!-- THIS IS FOR Bootstrap core CSS AND JS -->
          	     
<!--         <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"> -->
<!--         <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script> -->  		
	  		<link href="twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>	
	  		<script src="twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
	  			
	  			 <!-- THIS IS FOR FOOTABLE  CSS AND JS -->
	        <link href="css/plugins/footable/footable.core.css" rel="stylesheet" type="text/css"/> 
<!-- 	              THIS  footable.all.min.js ejecuta la Busqueda Total  id="filter" Buscar -->
	        <script src="js/plugins/footable/footable.all.min.js" type="text/javascript"></script>    
	        
	        
	        
	        
</head>
<body>


<!--  FORMULARIO LISTAR Y EDITAR col-sm-9  --> 
		<div class="col-sm-12">			
<!-- 			<div class="card-body"> -->
			 <div class="table-responsive">
			<table class="footable table table-striped table-hover" data-page-size="20" data-filter="#filter">
<!-- 		<table class="table table-hover"> -->

					<thead>
						<tr>
							<th>IDENTIFIER</th>
							<th>NOMBRE</th>
							<th>APELLIDO</th>
							<th>DNI</th>
							<th>ENABLED</th>
							<th>FECHA REGISTRO</th>							
						</tr>
					</thead>
										
					<tbody>
							<c:forEach var="em" items="${empleados}">
							<tr>							
								
									<td>${em.getIdentifier()}</td>
									<td>${em.getNombre()}</td>
									<td>${em.getApellido()}</td>
									<td>${em.getDni()}</td>
									<td>${em.getEnabled()}</td>
									<td>${em.getIssueDate()}</td>
																																			
								<td>
<%-- 								<a class="btn btn-warning" href="Controlador?menu=Empleado&accion=ListarByID&id=${em.getIdentifier()}" >Editar</a> --%>
<%-- 								<a class="btn btn-danger" href="Controlador?menu=Empleado&accion=Delete&id=${em.getIdentifier()}">Delete</a> --%>
 
								</td>
							</tr>
							</c:forEach>	


					</tbody>
					
					   <!-- CODIGO DE PAGINACION --> 
		            <tfoot>
		                <tr>
		                    <td colspan="10">
		                        <ul class="pagination pull-right"></ul>
		                    </td>
		                </tr>                
		            </tfoot>
		            <!---------------------------> 
					
				</table>
			</div>
			
		</div>		
		
		
				<!-- FUNCION DE FOOTABLE -->          
            <script lang="javascript">           
             $(function(){
               $('.footable').footable();
              });                         
             </script>  			
            <!---------------------> 
            

</body>
</html>