
<!--   Para trabajar con   ->  c:forEach  <-   -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%-- <%@page import="modeloDAO.EmpleadoDAO"%> --%>
<%-- <%@ page import="modelo.Empleado"%> --%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.Iterator"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Empleado</title>

<%--   THIS IS FOR DatePicker--%>

<!--    THIS IS JQUERY   -->
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
	$(function() {
		// datetimepicker
		// datepicker
		$("#datepicker").datepicker({
			dateFormat : "yy-mm-dd",
			// 	                    dateFormat:"dd/mm/yy",   // is ok
			changeMonth : true,
			changeYear : true
		});
	});
</script>

<%--------  DATEPICKER -------------%>




<!-- THIS IS FOR Bootstrap core CSS AND JS -->

<!--         <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"> -->
<!--         <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script> -->
<link href="twitter-bootstrap/css/bootstrap.css" rel="stylesheet"
	type="text/css" />
<script src="twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>

<!-- THIS IS FOR FOOTABLE  CSS AND JS -->
<link href="css/plugins/footable/footable.core.css" rel="stylesheet"
	type="text/css" />
<!-- 	              THIS  footable.all.min.js ejecuta la Busqueda Total  id="filter" Buscar -->
<script src="js/plugins/footable/footable.all.min.js"
	type="text/javascript"></script>



<script type="text/javascript">
	function showTable() {
		document.getElementById('tableEMPLEADO').style.visibility = "visible";
	}
	function hideTable() {
		document.getElementById('tableEMPLEADO').style.visibility = "hidden";
	}

	function hideButton() {
		document.getElementById('btnAgregar').style.visibility = "hidden";
	}

	function cleartextboxes2() {
		document.getElementById("consultar").value = "";
		document.getElementById("consultar").focus();

	}

	function cleartextboxes1() {
		document.getElementById("filter").value = "";
		document.getElementById("filter").focus();

	}

	function llamarVariasFunct() {
		cleartextboxes1();
		hideButton();
	}
</script>



</head>
<body>

	<%-- 	 <%@include file="buscar.jsp" %>	  --%>

	<!--  FILTRO  DE BUSQUEDA WITH  target="myFrame2"  -->
	<!-- 	<form  target="myFrame2" class="navbar-form navbar-right mt-4 col-lg-4" action="Controlador?menu=Empleado" method="POST"  >	 -->
	<form class="navbar-form navbar-right mt-4 col-lg-4" action="Controlador?menu=Empleado" method="POST">		

		<div class="navbar-right mt-3 col-sm-3 m-b-xs">
			<div class="input-group">
				<input type="text" placeholder="Buscar"
					class="input-sm form-control" id="filter"> <span
					class="input-group-btn">
					<button type="button" class="btn btn-sm btn-primary">
						<i class="glyphicon glyphicon-search"></i> Buscar Todo
					</button> <!-- 		           <button type="button" class="btn btn-sm btn-default" onclick="cleartextboxes1();hideButton();" >						 -->
					<button type="button" class="btn btn-sm btn-default"
						onclick="cleartextboxes1();">

						<i class="glyphicon glyphicon-remove"></i> Clear
					</button>
				</span>
			</div>
		</div>
	</form>
	<!--  END FILTRO  DE BUSQUEDA -->



	<!--  FORMULARIO REGISTRAR Y EDITAR col-sm-3  -->

	<div class="d-flex">

		<div class="col-sm-3">
			<div class="card">
				<div class="card-body">
					<form action="Controlador?menu=Empleado" method="POST">
						<div class="form-group">
							<label>Identifier</label> <input type="text"
								value="${empleadoXD.getIdentifier()}" name="txtIdent"
								class="form-control" required="">
						</div>
						<div class="form-group">
							<label>Nombre</label> <input type="text"
								value="${empleadoXD.getNombre()}" name="txtNom"
								class="form-control" required="">
						</div>
						<div class="form-group">
							<label>Apellido</label> <input type="text"
								value="${empleadoXD.getApellido()}" name="txtApe"
								class="form-control" required="">
						</div>
						<div class="form-group">
							<label>Dni</label> <input type="text"
								value="${empleadoXD.getDni()}" name="txtDni"
								class="form-control" required="">
						</div>
						<div class="form-group">
							<label>Enabled</label> <input type="text"
								value="${empleadoXD.getEnabled()}" name="txtEnabled"
								class="form-control" required="">
						</div>

						<div class="form-group">
							<label>IssueDate</label> <input type="text"
								value="${empleadoXD.getIssueDate()}" name="txtDate"
								id="datepicker" class="form-control" required="">
						</div>

						<!-- 						<button name="accion" value="Agregar" class="btn btn-primary">Agregar</button>							 -->
						<!-- 						<button name="accion" value="Editar" class="btn btn-info">Actualizar</button> -->


						<input type="submit" name="accion" value="Agregar"	id="btnAgregar" class="btn btn-primary">
						<!-- 					  HIDE BUTON -->
						<input type="submit" name="accion" value="Actualizar"id="btnUpdate" class="btn btn-success">


					</form>
				</div>
			</div>
		</div>

		<%-- 		 <%@include file="listaempleado.jsp" %> --%>



		<!--  FORMULARIO LISTAR Y EDITAR col-sm-9  -->

		<div class="col-sm-9">
			<!-- 			<div class="card-body"> -->
			<div class="table-responsive">
				<table id='tableEMPLEADO'
					class="footable table table-striped table-hover"
					data-page-size="20" data-filter="#filter">
					<!-- 		<table class="table table-hover"> -->

					<thead>
						<tr>
							<th>IDENTIFIER</th>
							<th>NOMBRE</th>
							<th>APELLIDO</th>
							<th>DNI</th>
							<th>ENABLED</th>
							<th>FECHA REGISTRO</th>
						</tr>
					</thead>

					<tbody>
						<c:forEach var="em" items="${empleados}">
							<tr>

								<td>${em.getIdentifier()}</td>
								<td>${em.getNombre()}</td>
								<td>${em.getApellido()}</td>
								<td>${em.getDni()}</td>
								<td>${em.getEnabled()}</td>
								<td>${em.getIssueDate()}</td>

								<td><a class="btn btn-warning"
									href="Controlador?menu=Empleado&accion=ListarByID&id=${em.getIdentifier()}"
									onclick="hideButton();">Editar</a> <a class="btn btn-danger"
									
									href="Controlador?menu=Empleado&accion=Delete&id=${em.getIdentifier()}">Delete</a>

								</td>
							</tr>
						</c:forEach>


					</tbody>

					<!-- 					   CODIGO DE PAGINACION  -->
					<tfoot>
						<tr>
							<td colspan="10">
								<ul class="pagination pull-right"></ul>
							</td>
						</tr>
					</tfoot>
					<!-- 		            -----------------------  -->

				</table>
			</div>
		</div>

	</div>


	<!-- FUNCION DE FOOTABLE -->
	<script lang="javascript">
		$(function() {
			$('.footable').footable();
		});
	</script>
	<!--------------------->
	
	

	<!--------    LLAMA OTRO FORMULARIO  listaempleado.jsp   ------->
	<!--  		 <iframe name="myFrame2" width="100%" height="500px" frameborder="0"></iframe> -->


</body>
</html>